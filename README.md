# ZeroDistrato
Antes de prosseguir com o readme, por favor colocar o csv 'dados-selecao-zerodistrato.csv' na raiz do projeto.

[**Link para a modelagem da base**](https://www.lucidchart.com/invitations/accept/28303f73-c8ab-4459-8f78-1339e7098bc9)

### Dependências externas
Em um ambiente linux:

* docker

* python3

### Setup
Prepara o ambiente do projeto para a execução.

    > make prepare
    > source venv/bin/activate
    > pip install -r requirements.txt

### Migrações
Executa a criação das tabelas baseado nos modelos de classe definidos.

    > make migrations

### Popula a base de dados
Executa o comando do django para inserir dados na base a partir de um arquivo .csv

    > make populate

    Apenas se certificar de que o arquivo "dados-selecao-zerodistrato.csv" se encontra na raiz do projeto.
    Pois o make já o chama por comodidade.

### SQL

1- Conta quantos distratos ocorreram.

```sql
    select count(*) from core_contrato where datadistrato is not null
```

```sql
    select co.valor_contrato_total as valor_total, 
        sum(pa.valor_liquido) as valor_pago, 
        sum(pa.valor_liquido) - sum(pa.juros_mora+pa.jurospricesac) as total_amortizacao,
        co.valor_contrato_total - sum(pa.valor_liquido) - sum(pa.juros_mora + pa.jurospricesac) as saldo_devedor
    from core_contrato co,
        core_parcela pa
    where co.id = pa.contrato_id
    group by (co.id)
    order by co.id;
```
    Este ultimo, infelizmente não exibe dias inadimplente e valor inadimplente