FROM ubuntu

RUN apt-get update && apt-get install -y gnupg

RUN apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys B97B0AFCAA1A47F044F244A07FCC7D46ACCC4CF8

RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ precise-pgdg main" > /etc/apt/sources.list.d/pgdg.list

RUN apt-get install -y postgresql-10 postgresql-client-10 postgresql-contrib-10
RUN apt-get install -y postgresql-plpython3-10

USER postgres

RUN    /etc/init.d/postgresql start &&\
    psql --command "CREATE USER ronald WITH SUPERUSER PASSWORD 'ronald';" &&\
    createdb -O ronald zero_distrato

RUN echo "host all  all    0.0.0.0/0  md5" >> /etc/postgresql/10/main/pg_hba.conf

RUN echo "listen_addresses='*'" >> /etc/postgresql/10/main/postgresql.conf

EXPOSE 5432

VOLUME  ["/etc/postgresql", "/var/log/postgresql", "/var/lib/postgresql"]

CMD ["/usr/lib/postgresql/10/bin/postgres", "-D", "/var/lib/postgresql/10/main", "-c", "config_file=/etc/postgresql/10/main/postgresql.conf"]
