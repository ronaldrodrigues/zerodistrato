from django.db import models
from datetime import datetime
# Create your models here.

def normalize_datetime(string_date):
    try:
        return datetime.strptime(string_date.replace(' ', ''), '%d-%m-%Y')
    except:
        return None

def float_or_none(number):
    try:
        return float(number)
    except:
        return None

class Pessoa(models.Model):
    nome = models.CharField(max_length=255)
    datanascimento = models.DateField(null=True)
    nucpf = models.CharField(max_length=255)
    flsexo = models.CharField(max_length=255)
    estadocivil = models.CharField(max_length=255)
    endereco = models.CharField(max_length=255)
    complemento = models.CharField(max_length=255)
    bairro = models.CharField(max_length=255)
    cep = models.CharField(max_length=255)
    municipio = models.CharField(max_length=255)
    dataaniversario = models.DateField(null=True)


class Empreendimento(models.Model):
    nome = models.CharField(max_length=255)
    endereco = models.CharField(max_length=255)
    bairro = models.CharField(max_length=255)
    cep = models.CharField(max_length=255)
    municipio = models.CharField(max_length=255)
    categoria = models.CharField(max_length=255)

    def fill_by_csv_row(self, row):
        self.nome = row['NOME_EMPREENDIMENTO']
        self.endereco = row['END_EMPREEND']
        self.bairro = row['BAIRRO_EMPREEND']
        self.cep = row['CEP_EMPREEND']
        self.municipio = row['MUNICIPIO_EMPREEND']
        self.categoria = row['CATEGORIA']


class Conjuge(models.Model):
    pessoa = models.ForeignKey('Pessoa', on_delete=models.CASCADE)
    
    def save(self, *args, **kwargs):
        self.pessoa.save()
        super(Conjuge, self).save(*args, **kwargs)

    def fill_by_csv_row(self, row):
        self.pessoa = Pessoa.objects.create()
        self.pessoa.nome = row['CONJUGE']
        self.pessoa.datanascimento = normalize_datetime(row['DATANASCIMENTOCONJUGE'])
        self.pessoa.nucpf = row['NUCPFCONJUGE']
        return self


class Cliente(models.Model):
    codigo_cliente = models.IntegerField()
    nuincrest = models.CharField(max_length=255)
    pessoa = models.ForeignKey('Pessoa', on_delete=models.CASCADE)
    conjuge = models.ForeignKey('Conjuge', on_delete=models.CASCADE, null=True)

    def save(self, *args, **kwargs):
        self.pessoa.save()
        #self.conjuge.save()
        super(Cliente, self).save(*args, **kwargs)

    def get_by_person(self, *args, **kwargs):
        p = Pessoa.objects.filter(*args, **kwargs).first()
        if p:
            return Cliente.objects.filter(pessoa_id=p.id).first()
        return None

    def fill_by_csv_row(self, row):
        self.codigo_cliente = int(row['CODIGO_CLIENTE'])
        self.nuincrest = row['NUINCREST']
        self.pessoa = Pessoa.objects.create()
        self.pessoa.nome = row['NOME_CLIENTE']
        self.pessoa.datanascimento = normalize_datetime(row['DATANASCIMENTO'])
        self.pessoa.nucpf = row['NUCPF']
        self.pessoa.flsexo = row['FLSEXO']
        self.pessoa.estadocivil = row['ESTADOCIVIL']
        self.pessoa.endereco = row['ENDCLIENTE']
        self.pessoa.complemento = row['COMPCLIENTE']
        self.pessoa.bairro = row['BAIRROCLIENTE']
        self.pessoa.cep = row['CEP_CLIENTE']
        self.pessoa.municipio = row['MUNICIPIOCLIENTE']
        self.pessoa.dataaniversario = normalize_datetime(row['DATAANIVERSARIO'])

        return self


class Imovel(models.Model):
    tipo = models.CharField(max_length=255)
    area = models.CharField(max_length=255)
    nuunidade = models.CharField(max_length=255)

    def fill_by_csv_row(self, row):
        self.tipo = row['TIPOIMOVEL']
        self.area = row['AREA']
        self.nuunidade = row['NUUNIDADE']

class Contrato(models.Model):
    titulo = models.IntegerField()
    datadistrato = models.DateField(null=True)
    valor_contrato_total = models.FloatField()
    cdempreend = models.FloatField()
    empreendimento = models.ForeignKey('Empreendimento', on_delete=models.CASCADE)
    cliente = models.ForeignKey('Cliente', on_delete=models.CASCADE)
    imovel = models.ForeignKey('Imovel', on_delete=models.CASCADE)

    def fill_by_csv_row(self, row):
        self.titulo = row['TITULO']
        self.datadistrato = normalize_datetime(row['DATADISTRATO'])
        self.valor_contrato_total = row['VALOR_CONTRATO_TOTAL']
        self.cdempreend = row['CDEMPREEND']



class Parcela(models.Model):
    parcela = models.IntegerField()
    indice_correcao = models.CharField(max_length=255)
    tipo_correcao = models.CharField(max_length=255)
    percentual_juros = models.FloatField()
    tipojuros = models.CharField(max_length=255)
    datavencimento = models.DateField(null=True)
    valororiginalparcela = models.FloatField()
    saldodevedorparcela = models.FloatField()
    tipo_baixa = models.CharField(max_length=255)
    data_baixa = models.DateField(null=True)
    valor_bruto = models.FloatField()
    valor_desconto = models.FloatField()
    valor_liquido = models.FloatField()
    juros_mora = models.FloatField()
    multa_mora = models.FloatField()
    correcaomonetaria = models.FloatField()
    mes_base = models.IntegerField()
    jurospricesac = models.FloatField()
    contrato = models.ForeignKey('Contrato', on_delete=models.PROTECT)

    def fill_by_csv_row(self, row):
        self.parcela = row['PARCELA']
        self.indice_correcao = row['INDICE_CORRECAO']
        self.tipo_correcao = row['TIPOCORRECAO']
        self.percentual_juros = float_or_none(row['PERCENTUAL_JUROS'])
        self.tipojuros = row['TIPOJUROS']
        self.datavencimento = normalize_datetime(row['DATAVENCIMENTO'])
        self.valororiginalparcela = float_or_none(row['VALORORIGINALPARCELA'])
        self.saldodevedorparcela = float_or_none(row['SALDODEVEDORPARCELA'])
        self.tipo_baixa = row['TIPO_BAIXA']
        self.data_baixa = normalize_datetime(row['DATABAIXA'])
        self.valor_bruto = float_or_none(row['VALORBRUTO'])
        self.valor_desconto = float_or_none(row['VALORDESCONTO'])
        self.valor_liquido = float_or_none(row['VALORLIQUIDO'])
        self.juros_mora = float_or_none(row['JUROSMORA'])
        self.multa_mora = float_or_none(row['MULTAMORA'])
        self.correcaomonetaria = float_or_none(row['CORRECAOMONETARIA'])
        self.mes_base = row['MES_BASE']
        self.jurospricesac = float_or_none(row['JUROSPRICESAC'])
        # self.contrato = Contrato.objects.create()

        # self.contrato.fill_by_csv_row(row)
    
    def save(self, *args, **kwargs):
        # self.contrato.save()
        super(Parcela, self).save(*args, **kwargs)