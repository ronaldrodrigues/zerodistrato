from django.core.management.base import BaseCommand
from zerodistrato_backend.core import models
import csv

def safe_save_client(row):
    try:
        aux = models.Cliente().get_by_person(nome=row['NOME_CLIENTE'])
    except:
        aux = None
    # se ja existe na base, apenas saio
    if aux:
        return

    # salvando Cliente
    cliente = models.Cliente()
    cliente.fill_by_csv_row(row)

    conjuge = models.Conjuge()
    conjuge.fill_by_csv_row(row)
    conjuge.save()
    
    cliente.conjuge = conjuge
    cliente.save()

def safe_save_empreendimento(row):
    try:
        aux = models.Empreendimento.objects.get(nome=row['NOME_EMPREENDIMENTO'])
    except:
        aux = None
    # se ja existe na base, apenas saio
    if aux:
        return
    empreendimento = models.Empreendimento()
    empreendimento.fill_by_csv_row(row)
    empreendimento.save()

def safe_save_imovel(row):
    try:
        aux = models.Imovel.objects.get(nuunidade=row['NUUNIDADE'])
    except:
        aux = None
    # se ja existe na base, apenas saio
    if aux:
        return
    imovel = models.Imovel()
    imovel.fill_by_csv_row(row)
    imovel.save()

def safe_save_contrato(row):
    try:
        aux = models.Contrato.objects.get(titulo=row['TITULO'])
    except:
        aux = None
    # se ja existe na base, apenas saio
    if aux:
        return

    imovel = models.Imovel.objects.get(nuunidade=row['NUUNIDADE'])
    empreendimento = models.Empreendimento.objects.get(nome=row['NOME_EMPREENDIMENTO'])
    cliente = models.Cliente().get_by_person(nome=row['NOME_CLIENTE'])
    contrato = models.Contrato()
    contrato.fill_by_csv_row(row)
    contrato.imovel = imovel
    contrato.cliente = cliente
    contrato.empreendimento = empreendimento
    contrato.save()

def safe_save_parcela(row):
    # salvando parcela
    contrato = models.Contrato.objects.get(titulo=row['TITULO'])
    aux = models.Parcela.objects.filter(contrato=contrato, parcela=row['PARCELA']).first()

    if aux:
        return

    parcela = models.Parcela()
    parcela.fill_by_csv_row(row)
    parcela.contrato = contrato
    parcela.save()


class Command(BaseCommand):
    help = 'Comando para importação do csv para a base'

    def add_arguments(self, parser):
        parser.add_argument('caminho', type=str, help='Caminho para o arquivo CSV')

    def handle(self, *args, **kwargs):
        path = kwargs['caminho']

        with open(path, encoding='utf-8-sig') as mycsv:
            dict_csv = csv.DictReader(mycsv, delimiter=';')
            # dict_csv.__next__()
            for index, row in enumerate(dict_csv):
                normalized_row = {k: v.strip() for k, v in row.items()}
                try:
                    safe_save_client(normalized_row)
                    safe_save_empreendimento(normalized_row)
                    safe_save_imovel(normalized_row)
                    safe_save_contrato(normalized_row)
                    safe_save_parcela(normalized_row)
                except:
                    print('Erro ao inserir a linha: ', index)
