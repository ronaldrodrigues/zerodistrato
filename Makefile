docker-image:
	sudo docker build -t image_postgres .

docker-container:
	sudo docker run -ti -d --name zeroDistrato image_postgres

rm-all:
	sudo docker rm -f zeroDistrato
	sudo docker rmi image_postgres
	rm -rf venv

prepare-db:
	make docker-image && make docker-container

prepare-environ:
	python3 -m venv venv
	make dev-env

prepare:
	make prepare-db
	make prepare-environ

dev-env:
	echo postgres_host=`sudo docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' zeroDistrato` > .env
	echo postgres_user=ronald >> .env
	echo postgres_pass=ronald >> .env
	echo postgres_port=5432 >> .env
	echo postgres_db=zero_distrato >> .env
	echo PYTHON_PATH=. >> .env


migrations:
	python manage.py makemigrations
	python manage.py migrate

populate:
	python manage.py populate_database_csv dados-selecao-zerodistrato.csv
